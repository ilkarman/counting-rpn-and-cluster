import torch
import torchvision
import os
import numpy as np
import cv2
import itertools
import json
import glob
from PIL import Image


def tensor2img(input_image, imtype=np.uint8, colour_map=None):
    if not isinstance(input_image, np.ndarray):
        if isinstance(input_image, torch.Tensor):  # get the data from a variable
            image_tensor = input_image.data
        else:
            return input_image
        image_numpy = (
            image_tensor[0].cpu().float().numpy()
        )  # convert it into a numpy array
        if image_numpy.shape[0] == 1:  # grayscale to RGB
            image_numpy = np.tile(image_numpy, (3, 1, 1))
        image_numpy = (
            (np.transpose(image_numpy, (1, 2, 0)) + 1) / 2.0 * 255.0
        )  # post-processing: tranpose and scaling
    else:  # if it is a numpy array, do nothing
        image_numpy = input_image
    im = image_numpy.astype(imtype)
    if colour_map:
        im = cv2.applyColorMap(im, cv2.COLORMAP_JET)
    return im


class Penguins(torch.utils.data.Dataset):
    def __init__(
        self, penguin_path, split="SPIGa", width_px=512, height_px=385, transforms=None
    ):
        def return_image_with_nonzero_penguins(penguin_path, split):
            json_loc = os.path.join(
                penguin_path, "CompleteAnnotations_2016-07-11", split + ".json"
            )
            print(json_loc)

            with open(json_loc) as json_file:
                data = json.load(json_file)
            penguin_images = list()
            len_data = len(data["dots"])
            print("Total entries: {}".format(len_data))

            for idx in range(len_data):
                file = os.path.join(
                    penguin_path, split, data["dots"][idx]["imName"] + ".JPG"
                )
                file_anno = data["dots"][idx]["xy"]
                if file_anno:
                    penguins = np.mean(
                        [
                            int(len(file_anno[i]))
                            for i in range(len(file_anno))
                            if isinstance(file_anno[i], list)
                        ]
                    )
                if np.isnan(penguins):
                    continue

                # Consider at least 5 penguins per image
                if penguins >= 5:
                    penguin_images.append(file)

            return penguin_images

        if isinstance(split, str):
            self.penguin_locs = return_image_with_nonzero_penguins(penguin_path, split)
        elif isinstance(split, list):
            self.penguin_locs = list()
            for sp in split:
                self.penguin_locs.extend(return_image_with_nonzero_penguins(penguin_path, sp))
        else:
            raise Exception("Split should be a list of strings or a string")
        self.resize = (width_px, height_px)
        self.transforms = transforms
        print("Loaded {} penguin images".format(len(self.penguin_locs)))

    def __len__(self):
        return len(self.penguin_locs)

    def __getitem__(self, idx):
        im = Image.open(self.penguin_locs[idx])
        width, height = im.size
        # Crop out the top pixel border
        # left, top, right, bottom
        im = im.crop((0, 50, width, height-60))
        im = im.resize(self.resize)
        if self.transforms:
            im = self.transforms(im)
        return im